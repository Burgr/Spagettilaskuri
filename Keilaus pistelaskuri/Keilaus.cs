﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keilaus_pistelaskuri
{
    public class Keilaus
    {
        int temp = 0; // Ensimmäinen heitto
        int temp2 = 0; // Toinen heitto
        int total = 0; // Pisteiden yhteenlasku
        int i = 0;
        int r1 = 0; // r1-10 = kierrokset
        int h1 = 0; // h1-10 = Paikkocounter
        int h11 = 0; // h11-h1010 = Täyskaatocounter
        int r2 = 0;
        int h2 = 0;
        int h22 = 0;
        int r3 = 0;
        int h3 = 0;
        int h33 = 0;
        int r4 = 0;
        int h4 = 0;
        int h44 = 0;
        int r5 = 0;
        int h5 = 0;
        int h55 = 0;
        int r6 = 0;
        int h6 = 0;
        int h66 = 0;
        int r7 = 0;
        int h7 = 0;
        int h77 = 0;
        int r8 = 0;
        int h8 = 0;
        int h88 = 0;
        int r9 = 0;
        int h9 = 0;
        int h99 = 0;
        int r10 = 0;
        int h10 = 0;
        int h1010 = 0;
        int r11 = 0;
        int r12 = 0;

        public void Pistelasku()
        {
            while (i < 10)
            {
                i++;
                temp = Convert.ToInt32(Console.ReadLine());

                // Eka heitto

                if (i == 1 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (temp + temp2 < 10) { r1 = temp + temp2; }
                    if (temp + temp2 == 10) { r1 = temp + temp2; h1 = 1; }
                }

                if (i == 1 && temp == 10) { r1 = temp; h11 = 1; }

                // Toka heitto
                if (i == 2 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h1 > 0 || h11 > 0)
                    {
                        if (h1 > 0 && temp + temp2 < 10) { r2 = temp + temp2; r1 = r1 + temp; h1 = 0; }
                        if (h11 > 0 && temp + temp2 < 10) { r2 = temp + temp2; r1 = r1 + temp + temp2; h11 = 0; }
                        if (h1 > 0 && temp + temp2 == 10) { r2 = temp + temp2; r1 = r1 + temp; h1 = 0; h2 = 1; }
                        if (h11 > 0 && temp + temp2 == 10) { r2 = temp + temp2; r1 = r1 + temp + temp2; h11 = 0; h2 = 1; }
                    }
                    if (temp + temp2 < 10 && h1 == 0 && h11 == 0) { r2 = temp + temp2; }
                    if (temp + temp2 == 10 && h1 == 0 && h11 == 0) { r2 = temp + temp2; h2 = 1; }
                }

                if (i == 2 && temp == 10 && h1 == 0 && h11 == 0) { r2 = temp; h22 = 1; }
                if (i == 2 && temp == 10 && h1 == 1) { r2 = temp; r1 = r1 + temp; h22 = 1; h1 = 0; }
                if (i == 2 && temp == 10 && h11 == 1) { r2 = temp; r1 = r1 + temp; h22 = 1; }

                // Kolmas heitto
                if (i == 3 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h11 > 0 || h2 > 0 || h22 > 0)
                    {
                        if (h11 > 0 && h22 > 0 && temp + temp2 < 10) { r3 = temp + temp2; r2 = r2 + temp + temp2; r1 = r1 + temp; h22 = 0; h11 = 0; }
                        if (h2 > 0 && temp + temp2 < 10) { r3 = temp + temp2; r2 = r2 + temp; h2 = 0; }
                        if (h22 > 0 && temp + temp2 < 10) { r3 = temp + temp2; r2 = r2 + temp + temp2; h22 = 0; }
                        if (h2 > 0 && temp + temp2 == 10) { r3 = temp + temp2; r2 = r2 + temp; h2 = 0; h3 = 1; }
                        if (h22 > 0 && temp + temp2 == 10) { r3 = temp + temp2; r2 = r2 + temp + temp2; h22 = 0; h3 = 1; }
                    }
                    if (temp + temp2 < 10 && h2 == 0 && h22 == 0) { r3 = temp + temp2; }
                    if (temp + temp2 == 10 && h2 == 0 && h22 == 0) { r3 = temp + temp2; h3 = 1; }
                }

                if (i == 3 && temp == 10 && h11 == 1 && h22 == 1 && h2 == 0) { r3 = temp; r2 = r2 + temp; r1 = r1 + temp; h11 = 0; h33 = 1; }
                if (i == 3 && temp == 10 && h2 == 0 && h22 == 0) { r3 = temp; h33 = 1; }
                if (i == 3 && temp == 10 && h2 == 1 && h33 == 0) { r3 = temp; r2 = r2 + temp; h33 = 1; h2 = 0; }
                if (i == 3 && temp == 10 && h22 == 1 && h33 == 0) { r3 = temp; r2 = r2 + temp; h33 = 1; }

                // Neljäs heitto
                if (i == 4 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h22 > 0 || h3 > 0 || h33 > 0)
                    {
                        if (h22 > 0 && h33 > 0 && temp + temp2 < 10) { r4 = temp + temp2; r3 = r3 + temp + temp2; r2 = r2 + temp; h33 = 0; h22 = 0; }
                        if (h3 > 0 && temp + temp2 < 10) { r4 = temp + temp2; r3 = r3 + temp; h3 = 0; }
                        if (h33 > 0 && temp + temp2 < 10) { r4 = temp + temp2; r3 = r3 + temp + temp2; h33 = 0; }
                        if (h3 > 0 && temp + temp2 == 10) { r4 = temp + temp2; r3 = r3 + temp; h3 = 0; h4 = 1; }
                        if (h33 > 0 && temp + temp2 == 10) { r4 = temp + temp2; r3 = r3 + temp + temp2; h33 = 0; h4 = 1; }
                    }
                    if (temp + temp2 < 10 && h3 == 0 && h33 == 0) { r4 = +temp + temp2; }
                    if (temp + temp2 == 10 && h3 == 0 && h33 == 0) { r4 = temp + temp2; h4 = 1; }
                }

                if (i == 4 && temp == 10 && h22 == 1 && h33 == 1 && h3 == 0) { r4 = temp; r3 = r3 + temp; r2 = r2 + temp; h22 = 0; h44 = 1; }
                if (i == 4 && temp == 10 && h3 == 0 && h33 == 0) { r4 = temp; h44 = 1; }
                if (i == 4 && temp == 10 && h3 == 1 && h44 == 0) { r4 = temp; r3 = r3 + temp; h44 = 1; h3 = 0; }
                if (i == 4 && temp == 10 && h33 == 1 && h44 == 0) { r4 = temp; r3 = r3 + temp; h44 = 1; }

                // Viides heitto
                if (i == 5 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h33 > 0 || h4 > 0 || h44 > 0)
                    {
                        if (h33 > 0 && h44 > 0 && temp + temp2 < 10) { r5 = temp + temp2; r4 = r4 + temp + temp2; r3 = r3 + temp; h44 = 0; h33 = 0; }
                        if (h4 > 0 && temp + temp2 < 10) { r5 = temp + temp2; r4 = r4 + temp; h4 = 0; }
                        if (h44 > 0 && temp + temp2 < 10) { r5 = temp + temp2; r4 = r4 + temp + temp2; h44 = 0; }
                        if (h4 > 0 && temp + temp2 == 10) { r5 = temp + temp2; r4 = r4 + temp; h4 = 0; h5 = 1; }
                        if (h44 > 0 && temp + temp2 == 10) { r5 = temp + temp2; r4 = r4 + temp + temp2; h44 = 0; h5 = 1; }
                    }
                    if (temp + temp2 < 10 && h4 == 0 && h44 == 0) { r5 = temp + temp2; }
                    if (temp + temp2 == 10 && h4 == 0 && h44 == 0) { r5 = temp + temp2; h5 = 1; }
                }

                if (i == 5 && temp == 10 && h33 == 1 && h44 == 1 && h4 == 0) { r5 = temp; r4 = r4 + temp; r3 = r3 + temp; h33 = 0; h55 = 1; }
                if (i == 5 && temp == 10 && h4 == 0 && h44 == 0) { r5 = temp; h55 = 1; }
                if (i == 5 && temp == 10 && h4 == 1 && h55 == 0) { r5 = temp; r4 = r4 + temp; h55 = 1; h4 = 0; }
                if (i == 5 && temp == 10 && h44 == 1 && h55 == 0) { r5 = temp; r4 = r4 + temp; h55 = 1; }

                // Kuudes heitto
                if (i == 6 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h44 > 0 || h5 > 0 || h55 > 0)
                    {
                        if (h44 > 0 && h55 > 0 && temp + temp2 < 10) { r6 = temp + temp2; r5 = r5 + temp + temp2; r4 = r4 + temp; h55 = 0; h44 = 0; }
                        if (h5 > 0 && temp + temp2 < 10) { r6 = temp + temp2; r5 = r5 + temp; h5 = 0; }
                        if (h55 > 0 && temp + temp2 < 10) { r6 = temp + temp2; r5 = r5 + temp + temp2; h55 = 0; }
                        if (h5 > 0 && temp + temp2 == 10) { r6 = temp + temp2; r5 = r5 + temp; h5 = 0; h6 = 1; }
                        if (h55 > 0 && temp + temp2 == 10) { r6 = temp + temp2; r5 = r5 + temp + temp2; h55 = 0; h6 = 1; }
                    }
                    if (temp + temp2 < 10 && h5 == 0 && h55 == 0) { r6 = temp + temp2; }
                    if (temp + temp2 == 10 && h5 == 0 && h55 == 0) { r6 = temp + temp2; h6 = 1; }
                }

                if (i == 6 && temp == 10 && h44 == 1 && h55 == 1 && h5 == 0) { r6 = temp; r5 = r5 + temp; r4 = r4 + temp; h44 = 0; h66 = 1; }
                if (i == 6 && temp == 10 && h5 == 0 && h55 == 0) { r6 = temp; h66 = 1; }
                if (i == 6 && temp == 10 && h5 == 1 && h66 == 0) { r6 = temp; r5 = r5 + temp; h66 = 1; h5 = 0; }
                if (i == 6 && temp == 10 && h55 == 1 && h66 == 0) { r6 = temp; r5 = r5 + temp; h66 = 1; }

                // Seitsemäs heitto
                if (i == 7 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h55 > 0 || h6 > 0 || h66 > 0)
                    {
                        if (h55 > 0 && h66 > 0 && temp + temp2 < 10) { r7 = temp + temp2; r6 = r6 + temp + temp2; r5 = r5 + temp; h66 = 0; h55 = 0; }
                        if (h6 > 0 && temp + temp2 < 10) { r7 = temp + temp2; r6 = r6 + temp; h6 = 0; }
                        if (h66 > 0 && temp + temp2 < 10) { r7 = temp + temp2; r6 = r6 + temp + temp2; h66 = 0; }
                        if (h6 > 0 && temp + temp2 == 10) { r7 = temp + temp2; r6 = r6 + temp; h6 = 0; h7 = 1; }
                        if (h66 > 0 && temp + temp2 == 10) { r7 = temp + temp2; r6 = r6 + temp + temp2; h66 = 0; h7 = 1; }
                    }
                    if (temp + temp2 < 10 && h6 == 0 && h66 == 0) { r7 = temp + temp2; }
                    if (temp + temp2 == 10 && h6 == 0 && h66 == 0) { r7 = temp + temp2; h7 = 1; }
                }

                if (i == 7 && temp == 10 && h55 == 1 && h66 == 1 && h6 == 0) { r7 = temp; r6 = r6 + temp; r5 = r5 + temp; h55 = 0; h77 = 1; }
                if (i == 7 && temp == 10 && h6 == 0 && h66 == 0) { r7 = temp; h77 = 1; }
                if (i == 7 && temp == 10 && h6 == 1 && h77 == 0) { r7 = temp; r6 = r6 + temp; h77 = 1; h6 = 0; }
                if (i == 7 && temp == 10 && h66 == 1 && h77 == 0) { r7 = temp; r6 = r6 + temp; h77 = 1; }

                // Kahdeksas heitto
                if (i == 8 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h66 > 0 || h7 > 0 || h77 > 0)
                    {
                        if (h66 > 0 && h77 > 0 && temp + temp2 < 10) { r8 = temp + temp2; r7 = r7 + temp + temp2; r6 = r6 + temp; h77 = 0; h66 = 0; }
                        if (h7 > 0 && temp + temp2 < 10) { r8 = temp + temp2; r7 = r7 + temp; h7 = 0; }
                        if (h77 > 0 && temp + temp2 < 10) { r8 = temp + temp2; r7 = r7 + temp + temp2; h77 = 0; }
                        if (h7 > 0 && temp + temp2 == 10) { r8 = temp + temp2; r7 = r7 + temp; h7 = 0; h8 = 1; }
                        if (h77 > 0 && temp + temp2 == 10) { r8 = temp + temp2; r7 = r7 + temp + temp2; h77 = 0; h8 = 1; }
                    }
                    if (temp + temp2 < 10 && h7 == 0 && h77 == 0) { r8 = temp + temp2; }
                    if (temp + temp2 == 10 && h7 == 0 && h77 == 0) { r8 = temp + temp2; h8 = 1; }
                }

                if (i == 8 && temp == 10 && h66 == 1 && h77 == 1 && h7 == 0) { r8 = temp; r7 = r7 + temp; r6 = r6 + temp; h66 = 0; h88 = 1; }
                if (i == 8 && temp == 10 && h7 == 0 && h77 == 0) { r8 = temp; h88 = 1; }
                if (i == 8 && temp == 10 && h7 == 1 && h88 == 0) { r8 = temp; r7 = r7 + temp; h88 = 1; h7 = 0; }
                if (i == 8 && temp == 10 && h77 == 1 && h88 == 0) { r8 = temp; r7 = r7 + temp; h88 = 1; }

                // Yheksäs heitto
                if (i == 9 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h77 > 0 || h8 > 0 || h88 > 0)
                    {
                        if (h77 > 0 && h88 > 0 && temp + temp2 < 10) { r9 = temp + temp2; r8 = r8 + temp + temp2; r7 = r7 + temp; h88 = 0; h77 = 0; }
                        if (h8 > 0 && temp + temp2 < 10) { r9 = temp + temp2; r8 = r8 + temp; h8 = 0; }
                        if (h88 > 0 && temp + temp2 < 10) { r9 = temp + temp2; r8 = r8 + temp + temp2; h88 = 0; }
                        if (h8 > 0 && temp + temp2 == 10) { r9 = temp + temp2; r8 = r8 + temp; h8 = 0; h9 = 1; }
                        if (h88 > 0 && temp + temp2 == 10) { r9 = temp + temp2; r8 = r8 + temp + temp2; h88 = 0; h9 = 1; }
                    }
                    if (temp + temp2 < 10 && h8 == 0 && h88 == 0) { r9 = temp + temp2; }
                    if (temp + temp2 == 10 && h8 == 0 && h88 == 0) { r9 = temp + temp2; h9 = 1; }
                }

                if (i == 9 && temp == 10 && h77 == 1 && h88 == 1 && h8 == 0) { r9 = temp; r8 = r8 + temp; r7 = r7 + temp; h77 = 0; h99 = 1; }
                if (i == 9 && temp == 10 && h8 == 0 && h88 == 0) { r9 = temp; h99 = 1; }
                if (i == 9 && temp == 10 && h8 == 1 && h99 == 0) { r9 = temp; r8 = r8 + temp; h99 = 1; h8 = 0; }
                if (i == 9 && temp == 10 && h88 == 1 && h99 == 0) { r9 = temp; r8 = r8 + temp; h99 = 1; }

                // Kymmenes heitto
                if (i == 10 && temp < 10)
                {
                    temp2 = Convert.ToInt32(Console.ReadLine());
                    if (h88 > 0 || h9 > 0 || h99 > 0)
                    {
                        if (h88 > 0 && h99 > 0 && temp + temp2 < 10) { r10 = temp + temp2; r9 = r9 + temp + temp2; r8 = r8 + temp; h99 = 0; h88 = 0; }
                        if (h9 > 0 && temp + temp2 < 10) { r10 = temp + temp2; r9 = r9 + temp; h9 = 0; }
                        if (h99 > 0 && temp + temp2 < 10) { r10 = temp + temp2; r9 = r9 + temp + temp2; h99 = 0; }
                        if (h9 > 0 && temp + temp2 == 10) { r10 = temp + temp2; r9 = r9 + temp; h9 = 0; h10 = 1; }
                        if (h99 > 0 && temp + temp2 == 10) { r10 = temp + temp2; r9 = r9 + temp + temp2; h99 = 0; h10 = 1; }
                    }
                    if (temp + temp2 < 10 && h9 == 0 && h99 == 0) { r10 = temp + temp2; }
                    if (temp + temp2 == 10 && h9 == 0 && h99 == 0) { r10 = temp + temp2; h10 = 1; }
                }

                if (i == 10 && temp == 10 && h88 == 1 && h99 == 1 && h9 == 0) { r10 = temp; r9 = r9 + temp; r8 = r8 + temp; h88 = 0; h1010 = 1; }
                if (i == 10 && temp == 10 && h9 == 0 && h99 == 0) { r10 = temp; h1010 = 1; }
                if (i == 10 && temp == 10 && h9 == 1 && h1010 == 0) { r10 = temp; r9 = r9 + temp; h1010 = 1; h9 = 0; }
                if (i == 10 && temp == 10 && h99 == 1 && h1010 == 0) { r10 = temp; r9 = r9 + temp; h1010 = 1; }

                if (i == 10 && h10 == 1 && h99 == 0) { temp = Convert.ToInt32(Console.ReadLine()); r11 = temp; h10 = 0; }

                if (i == 10 && h1010 == 1)
                {
                    temp = Convert.ToInt32(Console.ReadLine());
                    if (h99 == 0)
                    {
                        r11 = temp;
                        temp = Convert.ToInt32(Console.ReadLine());
                        r12 = temp;
                    }
                    if (h99 == 1)
                    {
                        r11 = temp; r9 = r9 + temp; h99 = 0;
                        temp = Convert.ToInt32(Console.ReadLine());
                        r12 = temp;
                    }

                }
                total = r1 + r2 + r3 + r4 + r5 + r6 + r7 + r8 + r9 + r10 + r11 + r12;
                Console.WriteLine("Pistemäärä: " + total);
            }
        }
    }
}